<?php
// Settings
// host, user and password settings
$host = "localhost";
$user = "logger";
$password = "logsql";
$database = "temperatures";

//how many hours backwards do you want results to be shown in web page.
$hours = 72;

// make connection to database
$connectdb = mysqli_connect($host,$user,$password)
or die ("Cannot reach database");

// select db
mysqli_select_db($connectdb,$database)
or die ("Cannot select database");

// sql command that selects all entires from current time and X hours backwards
$sql="SELECT * FROM temperaturedata WHERE dateandtime >= (NOW() - INTERVAL $hours HOUR) order by dateandtime desc";

$sqloutdoortemp="SELECT temperature FROM temperaturedata WHERE dateandtime >= (NOW() - INTERVAL $hours HOUR) AND sensor = 'Outside' ORDER BY dateandtime desc";
$sqloutdoorhum="SELECT humidity FROM temperaturedata WHERE dateandtime >= (NOW() - INTERVAL $hours HOUR) AND sensor = 'Outside' ORDER BY dateandtime desc";
$sqlindoortemp="SELECT temperature FROM temperaturedata WHERE dateandtime >= (NOW() - INTERVAL $hours HOUR) AND sensor = 'Inside' ORDER BY dateandtime desc";
$sqlindoorhum="SELECT humidity FROM temperaturedata WHERE dateandtime >= (NOW() - INTERVAL $hours HOUR) AND sensor = 'Inside' ORDER BY dateandtime desc";
$sqltime="SELECT dateandtime FROM temperaturedata WHERE dateandtime >= (NOW() - INTERVAL $hours HOUR) AND sensor ='Inside' ORDER BY dateandtime desc";


//$sql="select * from temperaturedata where date(dateandtime) = curdate();";

// set query to variable
$tables = mysqli_query($connectdb,$sql);
$outdoortemp = mysqli_query($connectdb,$sqloutdoortemp);
$outdoorhum = mysqli_query($connectdb,$sqloutdoorhum);
$indoortemp = mysqli_query($connectdb,$sqlindoortemp);
$indoorhum = mysqli_query($connectdb,$sqlindoorhum);
$times = mysqli_query($connectdb,$sqltime);

$timedata = Array();
while ( $timetest = mysqli_fetch_assoc($times) ) {
	$timedata[] = $timetest['dateandtime'];
}
$timedata = array_reverse($timedata);

$outdoortempdata = Array();
while ( $outdoortemptest = mysqli_fetch_assoc($outdoortemp)){
	//$outdoortempdata[] = $outdoortemptest['temperature'];
}
$outdoortempdata = array_reverse($outdoortempdata);

$outdoorhumdata = array();
while ($outdoorhumtest = mysqli_fetch_assoc($outdoorhum)){
	//$outdoorhumdata[] = $outdoorhumtest['humidity'];
}
$outdoorhumdata = array_reverse($outdoorhumdata);

$indoortempdata = array();
while ( $indoortemptest = mysqli_fetch_assoc($indoortemp)){
	$indoortempdata[] = $indoortemptest['temperature'];
}
$indoortempdata = array_reverse($indoortempdata);

$indoorhumdata = array();
while($indoorhumtest = mysqli_fetch_assoc($indoorhum)){
	$indoorhumdata[] = $indoorhumtest['humidity'];
}
$indoorhumdata = array_reverse($indoorhumdata);


// create content to web page ?> <html>

<head>
<title>Jeff's Thermometer</title>
</head>

<body>
<div>
<canvas id="myChart" width="1280" height="640"></canvas>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var timelabels = <?=json_encode($timedata)?>;
var outdoortempplot = <?=json_encode($outdoortempdata)?>;
var indoortempplot = <?=json_encode($indoortempdata)?>;
var outdoorhumplot = <?=json_encode($outdoorhumdata)?>;
var indoorhumplot = <?=json_encode($indoorhumdata)?>;

var T = new Array();
for (var k = 5; k < timelabels.length; k=k+5){
    T.push(timelabels[k-5]);
}

var ODT = new Array();
for (var k = 5; k < outdoortempplot.length; k=k+5){
    var sum = 0;
    for (var i = k-5; i < k; i++) {
	sum += Number(outdoortempplot[i]);
    }
    ODT.push(Math.round(sum/5 * 100) / 100);
}

var IDT = new Array();
for (var k = 5; k < indoortempplot.length; k=k+5){
    var sum = 0;
    for (var i = k-5; i < k; i++) {
	sum += Number(indoortempplot[i]);
    }
    IDT.push(Math.round(sum/5 * 100) / 100);
}

var ODH = new Array();
for (var k = 5; k < outdoorhumplot.length; k=k+5){
    var sum = 0;
    for (var i = k-5; i < k; i++) {
	sum += Number(outdoorhumplot[i]);
    }
    ODH.push(Math.round(sum/5 * 100) / 100);
}

var IDH = new Array();
for (var k = 5; k < indoorhumplot.length; k=k+5){
    var sum = 0;
    for (var i = k-5; i < k; i++) {
	sum += Number(indoorhumplot[i]);
    }
    IDH.push(Math.round(sum/5 * 100) / 100);
}

var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: T,
        datasets: [
	//~ {
            //~ label: 'Outdoor Temp',
  //~ //        backgroundColor: 'rgb(255, 99, 132)',
            //~ borderColor: 'rgb(255, 99, 132)',
            //~ data: ODT,
	    //~ yAxisID: 'left-y-axis'
        //~ },
	{
	    label: 'Indoor Temp',
  //	    backgroundColor: 'rgb(99, 132, 255)',
	    borderColor: 'rgb(99, 132, 255)',
	    data: IDT,
	    yAxisID: 'left-y-axis'
	},
	//~ {
	    //~ label: 'Outdoor Humidity',
	    //~ borderColor: 'rgb(132, 255, 99)',
	    //~ data: ODH,
	    //~ yAxisID: 'right-y-axis'
	//~ },
	{
	    label: 'Indoor Humidity',
	    borderColor: 'rgb(255, 132, 99)',
	    data: IDH,
	    yAxisID: 'right-y-axis'
	}],
    },

    // Configuration options go here
    options: {
	elements: {
	    line: {
		tension: 0.4
	    },
	    point: {
		radius: 0,
		hitRadius: 10,
		hoverRadius: 10
	    }
	},
	scales: {
		yAxes: [{
		    id: 'left-y-axis',
		    type: 'linear',
		    position: 'left',
		    label: 'Temperature (Degrees F)'
		}, {
		    id: 'right-y-axis',
		    type: 'linear',
		    position: 'right',
		    label: '% Humidity'
		}]
	}
    }
});
</script>
</body>
<br><br>
<center>Data</center>
<br><br>
<table width="800" border="1" cellpadding="1" cellspacing="1" align="center">
<tr>
<th>Date</th>
<th>Sensor</th>
<th>Temperature</th>
<th>Humidity</th>
<tr>
<?php
// loop all the results that were read from database and "draw" to web page
$i = 0;
while($table=mysqli_fetch_assoc($tables)){
	echo "<tr>";
	echo "<td>".$table['dateandtime']."</td>";
	echo "<td>".$table['sensor']."</td>";
	echo "<td>".$table['temperature']."</td>";
	echo "<td>".$table['humidity']."</td>";
	echo "<tr>";
	
}
?>
</table>
</html>

