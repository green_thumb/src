#!/usr/bin/env python3

import cpe185io

def run():
    cpe185io.tempFan()
    cpe185io.waterPump()
    cpe185io.lightBulb()
    cpe185io.buttonValve()
    cpe185io.time.sleep(0.1)
    return;


try:
    while (True):
        run()
        
except: KeyboardInterrupt   
