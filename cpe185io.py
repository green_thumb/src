#!/usr/bin/env python3
## IO controller - Jeff Harthorn

import time
import datetime
import RPi.GPIO as GPIO
import Adafruit_DHT

moisture = 17
temp = 18
ambient = 27
button = 22

pump = 25
fan = 23
bulb = 24
valve = 5

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)
GPIO.setup(moisture, GPIO.IN)
GPIO.setup(temp, GPIO.IN)
GPIO.setup(ambient, GPIO.IN)
GPIO.setup(button, GPIO.IN)
GPIO.setup(pump, GPIO.OUT)
GPIO.setup(fan, GPIO.OUT)
GPIO.setup(bulb, GPIO.OUT)
GPIO.setup(valve, GPIO.OUT) 

sensor = Adafruit_DHT.DHT22
now = datetime.datetime.now()
GPIO.output(valve, False)

## process temp sensor, fan
def tempFan():
    humidity, temperature = Adafruit_DHT.read(sensor, temp)
    if humidity is not None and temperature is not None:
        temperature = temperature * 1.8 + 32
        print ('{0:.2f}'.format(temperature), 'F')
        print ('{0:.2f}'.format(humidity), '%')
        if (temperature > 120 or temperature < 70):
            GPIO.output(fan, True)
        elif (humidity > 100 or humidity < 40):
            GPIO.output(fan, True)
        else:
            GPIO.output(fan, False)
    else:
        print ('Failed to get a reading. Please try again!')
    return;
	
## process ambient sensor, light bulb  
def lightBulb():         
    if not GPIO.input(ambient): ## not light = dark
        GPIO.output(bulb, True)
    else:    
        GPIO.output(bulb, False)
    return;

## process moisture sensor, water pump  
def waterPump():
    if GPIO.input(moisture) and (not GPIO.input(valve)):
        GPIO.output(pump, True)
    else:
        GPIO.output(pump, False)
        
## process button signal, valve signal  
def buttonValve(): 

    if GPIO.input(valve) and GPIO.input(button):
        GPIO.output(valve, False)
    elif GPIO.input(button):
        GPIO.output(valve, True)   
